package com.montanez.bottombar;

import static android.content.ContentValues.TAG;

import android.Manifest;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;


public class MapsFragment extends Fragment {

    MapView mapa;
    IMapController mapaControlador;;
    MyLocationNewOverlay miUbicacion;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public MapsFragment() {
        // Required empty public constructor
    }


    public static MapsFragment newInstance(String param1, String param2) {
        MapsFragment fragment = new MapsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vi = inflater.inflate(R.layout.fragment_maps, container, false);

            mapa = (MapView) vi.findViewById(R.id.mapa);

        //Obtener el contexto
        Context ctx = getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));

        //Tile: Teselas, baldosas,
        mapa.setTileSource(TileSourceFactory.MAPNIK);

        //Permite controlar zoom con dedos
        mapa.setMultiTouchControls(true);

        //Explícitamente pedir los permisos
        //Arreglos de cadadenos de texto en donde guardo los permisos

        String[] permisos= new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION,
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permisos,1);
        }

        //Colocar un marcador en el sitio que me marque el "GPS"
        //Controlar el mapa
        mapaControlador=mapa.getController();

        miUbicacion= new MyLocationNewOverlay(new GpsMyLocationProvider(ctx ), mapa);
        //Habilitar
        miUbicacion.enableMyLocation();
        mapa.getOverlays().add(miUbicacion);

        miUbicacion.runOnFirstFix(
                () -> {
                    runOnUiThread(
                            ()->{
                                mapaControlador.setZoom((double) 10);
                                mapaControlador.setCenter(miUbicacion.getMyLocation());
                                Log.d(TAG, "Ubicado!!!" + miUbicacion.getMyLocation().getLatitude() +
                                        miUbicacion.getMyLocation().getLongitude());
                            });
                }
        );



        return vi;
    }

}