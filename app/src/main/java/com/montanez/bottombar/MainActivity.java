package com.montanez.bottombar;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Switch;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

public class MainActivity extends AppCompatActivity {

    FrameLayout myContainerFragments;
    BottomNavigationView lowBar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lowBar = findViewById(R.id.lowMenu);
        lowBar.setOnItemSelectedListener(
                new NavigationBarView.OnItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem i) {

                        int  id = i.getItemId();
                        Log.d(TAG, String.valueOf(id));

                        switch(id){

                            case R.id.iTeams:
                                        getSupportFragmentManager().beginTransaction().replace(R.id.containerFragments, new TeamsFragment()).commit();
                                 break;
                            case R.id.iCalc:

                                 /*FragmentManager miAdministrador;
                                FragmentTransaction miFragmentTranstion;
                                miAdministrador = getSupportFragmentManager();
                                miFragmentTranstion = miAdministrador.beginTransaction();
                                miFragmentTranstion.replace(R.id.containerFragments, new calculatorFragment());
                                miFragmentTranstion.commit();
                                */
                                         getSupportFragmentManager().beginTransaction().replace(R.id.containerFragments, new calculatorFragment()).commit();
                                break;
                            case R.id.iMap:
                                        getSupportFragmentManager().beginTransaction().replace(R.id.containerFragments, new MapsFragment()).commit();
                                break;
                        }

                        return false;
                    }
                }
        );





    }
}